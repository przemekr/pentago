#ifndef PENTAGO_AI_H
#define PENTAGO_AI_H

#include "pentago.h"
#include "ai_minmax_search.h"
#include <utility>
#include <vector>
#include <algorithm>

class Pentago_ai: public Pentago
{
public:
   Pentago_ai() :Pentago()
   {
      isSubEmpty[0] = true;
      isSubEmpty[1] = true;
      isSubEmpty[2] = true;
      isSubEmpty[3] = true;
   }
   Pentago_ai(const Pentago_ai& other)
   {
      *this = other;
   }

   int evaluateSubSq(int sq, Point p)
   {
      int tx = topXSquare(sq); 
      int ty = topYSquare(sq); 
      int player = 0;
      int opponent = 0;
      Point o = (p == BLACK)?     WHITE: BLACK;

      player += (p == points[tx][ty] && p == points[tx+1][ty] && p == points[tx+2][ty])? 1:0;
      player += (p == points[tx][ty+1] && p == points[tx+1][ty+1] && p == points[tx+2][ty+1])? 1:0;
      player += (p == points[tx][ty+2] && p == points[tx+1][ty+2] && p == points[tx+2][ty+2])? 1:0;

      player += (p == points[tx][ty] && p == points[tx][ty+1] && p == points[tx][ty+2])? 1:0;
      player += (p == points[tx+1][ty] && p == points[tx+1][ty+1] && p == points[tx+1][ty+2])? 1:0;
      player += (p == points[tx+2][ty] && p == points[tx+2][ty+1] && p == points[tx+2][ty+2])? 1:0;

      player += (p == points[tx][ty] && p == points[tx+1][ty+1] && p == points[tx+2][ty+2])? 1:0;
      player += (p == points[tx][ty] && p == points[tx+1][ty+1] && p == points[tx+2][ty+2])? 1:0;

      opponent += (o == points[tx][ty] && o == points[tx+1][ty] && o == points[tx+2][ty])? 1:0;
      opponent += (o == points[tx][ty+1] && o == points[tx+1][ty+1] && o == points[tx+2][ty+1])? 1:0;
      opponent += (o == points[tx][ty+2] && o == points[tx+1][ty+2] && o == points[tx+2][ty+2])? 1:0;

      opponent += (o == points[tx][ty] && o == points[tx][ty+1] && o == points[tx][ty+2])? 1:0;
      opponent += (o == points[tx+1][ty] && o == points[tx+1][ty+1] && o == points[tx+1][ty+2])? 1:0;
      opponent += (o == points[tx+2][ty] && o == points[tx+2][ty+1] && o == points[tx+2][ty+2])? 1:0;

      opponent += (o == points[tx][ty] && o == points[tx+1][ty+1] && o == points[tx+2][ty+2])? 1:0;
      opponent += (o == points[tx][ty] && o == points[tx+1][ty+1] && o == points[tx+2][ty+2])? 1:0;

      return player - opponent;
   }


   int evaluate(int level)
   {
      Point p = (moveNr-level)%2? BLACK: WHITE;
      Point o = (p == BLACK)?     WHITE: BLACK;
      if (haveFive(o))
         return -100+level;
      if (haveFive(p))
         return  100-level;
      return 0 +
         evaluateSubSq(0, p) +
         evaluateSubSq(1, p) +
         evaluateSubSq(2, p) +
         evaluateSubSq(3, p) ;
   }

   Move* findNextPossible(Move* m, bool reduced = false)
   {
      for (int r = m->rotate; r <= ROTATE_RIGH; r++)
      {
         for (int s = m->square; s < SUB_SQUARES_NR; s++)
         {
            //if ((reduced || isEmpty(s)) and (s != 0 or r != ROTATE_LEFT))
            if ((reduced) and (s != 0 or r != ROTATE_LEFT))
            {
               continue;
            }

            for (int it = m->it+1; it < SIZE*SIZE; it++)
            {
               int i = movePerm[it]%6;
               int j = movePerm[it]/6;

               if (points[i][j] == EMPTY)
               {
                  m->square = s;
                  m->rotate = r;
                  m->first  = i;
                  m->second = j;
                  m->it     = it;
                  return m;
               }
            }
            m->it = -1;
         }
         m->square = 0;
      }
      return NULL;
   }

   bool isEmpty(int s)
   {
      if (not isSubEmpty[s])
         return false;
      for (int i = 0; i < SUB_SQUARES_SZ; i++)
         for (int j = 0; j < SUB_SQUARES_SZ; j++)
         {
            if (i == 1 && j == 1)
               continue;

            int si = topXSquare(s)+i; 
            int sj = topYSquare(s)+j; 
            if (points[si][sj] != EMPTY)
            {
               isSubEmpty[s] = false;
               return false;
            }
         }
      return true;
   }
   
   class iterator
   {
      Pentago_ai* b;
      Move* m;
      bool reduced;
   public:
      Move& operator *()
      {
         return *m;
      }
      void operator++(int)
      {
         m = b->findNextPossible(m, reduced);
      }
      bool operator ==(iterator o)
      {
         return o.m == m;
      }
      bool operator !=(iterator o)
      {
         return o.m != m;
      }
      iterator(Pentago_ai* board, Move* move, bool r = false): b(board), m(move), reduced(r) { }
      iterator(): b(NULL), m(NULL), reduced(false) {}
   };
   iterator begin(bool reduced = false)
   {
      if (endOfTheGame()) return iterator(this, NULL);
      m.it = -1;
      m.first = 0;
      m.second = 0;
      m.rotate = ROTATE_LEFT;
      m.square = 0;
      return iterator(this, findNextPossible(&m), reduced);
   }
   iterator end() { return iterator(this, NULL); }

   int noOfPossibleMoves()
   {
      updatePerm();
      int no = 0;
      for (iterator i = begin(); i != end(); i++) 
         no++;
      return no;
   }

   void updatePerm()
   {
      std::srand(time(NULL));
      movePerm.clear();
      movePerm.push_back(1 +6* 1);
      movePerm.push_back(1 +6* 4);
      movePerm.push_back(4 +6* 1);
      movePerm.push_back(4 +6* 4);
      for (int i = 0; i < SIZE*SIZE; ++i)
      {
         if (std::find(movePerm.begin(), movePerm.end(), i) == movePerm.end())
            movePerm.push_back(i);
      }
      std::random_shuffle (movePerm.begin(), movePerm.begin()+4);
      std::random_shuffle (movePerm.begin()+4, movePerm.end());
   }

private:
   Move m;
   bool isSubEmpty[4];
   static std::vector<int> movePerm;
};

Move ai_move(Pentago_ai b, int level, CbFun, void* cbFunData);
#endif
