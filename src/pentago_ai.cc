//#define DEBUG_AI (1)
#include "ai_minmax_search.h"
#include "pentago_ai.h"
#include "stdio.h"

long noEval;
int rating;
std::vector<int> Pentago_ai::movePerm;

Move ai_move(Pentago_ai b, int level, CbFun fun, void* cbFunData)
{
   b.updatePerm();

   Move m;
   m.first = 0;
   m.second = 0;
   m.square = 0;
   m.rotate = ROTATE_RIGH;
   noEval = 0;
   int reduced = 0;
   int limit   = 0;
   switch (level)
   {
      case 0:
         reduced = 4;
         limit   = 4;
         break;
      case 1:
         reduced = 2;
         limit = 4;
         break;
      case 2:
         reduced = 4;
         limit = 4;
         break;
      case 3:
         reduced = 4;
         limit = 5;
         break;
      case 4:
         reduced = 5;
         limit = 5;
         break;
      case 5:
      default:
         reduced = 6;
         limit = 6;
         break;
   }
   ai_min_max(b, m, limit, reduced, fun, cbFunData);
   return m;
}
