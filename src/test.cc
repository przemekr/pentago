#define DEBUG_AI (1)
#include "pentago_ai.h"
#include <unistd.h>

CbResult cbfun(void*)
{
   return CONTINUE;
};

int main(int argc, const char *argv[])
{
   Pentago_ai b;
   b.points[1][1] = WHITE;
   b.points[3][1] = WHITE;
   b.points[4][1] = WHITE;
   b.points[1][4] = BLACK;
   b.points[4][4] = BLACK;
   b.moveNr = 1;
   b.noOfPossibleMoves();
   std::cout << "" << b.to_str() << std::endl;

   // 4,3 sq2 R
   Move m = ai_move(b, 0, cbfun, NULL);
   b.move(m);
   std::cout << "" << b.to_str() << std::endl;
}
