#include "ctrl/agg_cbox_ctrl.h"

class MenuView : public View
{
public:
   MenuView(App& application): app(application),
   exitApp (40, 20,   150, 40,    "Quit App",  !flip_y),
   exitMenu(40, 60,   150, 80,    "Return  ",  !flip_y),
   newGame (40, 100,  150, 120,   "New Game",  !flip_y),
   music(220, 60,                "Music  ",  !flip_y),
   sound(220, 20,                "Sound  ",  !flip_y),
   p1_ctr  (80, 150,  320, 170,  !flip_y),
   p2_ctr  (80, 190,  320, 210,  !flip_y)
   {
      p1_ctr.range(0, 4);
      p1_ctr.num_steps(4);
      p1_ctr.value(app.p1level);
      p1_ctr.label("P1: H");
      p1_ctr.background_color(transp);
      p1_ctr.text_color(black);
      p1_ctr.text_thickness(CTRL_TEXT_THICKNESS);
      p1_ctr.border_width(0, 0);
      p2_ctr.range(0, 4);
      p2_ctr.num_steps(4);
      p2_ctr.value(app.p2level);
      p2_ctr.label("P2: H");
      p2_ctr.background_color(transp);
      p2_ctr.text_color(black);
      p2_ctr.text_thickness(CTRL_TEXT_THICKNESS);
      p2_ctr.border_width(0, 0);

      exitMenu.background_color(red);
      exitApp.background_color(red);
      newGame.background_color(red);

      sound.text_size(15);
      sound.text_color(red);
      sound.text_thickness(CTRL_TEXT_THICKNESS);
      sound.active_color(red);
      sound.inactive_color(red);

      music.text_size(15);
      music.text_color(red);
      music.text_thickness(CTRL_TEXT_THICKNESS);
      music.active_color(red);
      music.inactive_color(red);

      add_ctrl(sound);
      add_ctrl(music);

      add_ctrl(p1_ctr);
      add_ctrl(p2_ctr);
      add_ctrl(exitMenu);
      add_ctrl(exitApp);
      add_ctrl(newGame);
   }
   virtual void on_draw()
   {
      int w = app.rbuf_window().width();
      int h = app.rbuf_window().height();
      double scale = app.rbuf_window().width()/400.0;
      static agg::trans_affine shape_mtx; shape_mtx.reset();
      static agg::trans_affine img_mtx; img_mtx.reset();
      shape_mtx *= agg::trans_affine_scaling(scale);
      shape_mtx *= agg::trans_affine_translation(0, 0);

      img_mtx *= agg::trans_affine_scaling(scale);
      img_mtx *= agg::trans_affine_translation(0, 250);
      img_mtx.invert();

      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(lgray);

      typedef agg::span_interpolator_linear<agg::trans_affine> interpolator_type;
      interpolator_type interpolator(img_mtx);
      typedef agg::image_accessor_clone<pixfmt_type> img_accessor_type;
      pixfmt_type pixf_img(app.rbuf_img(0));
      img_accessor_type ia(pixf_img);
      typedef agg::span_image_filter_rgba_nn<img_accessor_type, interpolator_type> span_gen_type;
      span_gen_type sg(ia, interpolator);
      agg::span_allocator<color_type> sa;
      ras.move_to_d(0,250);
      ras.line_to_d(w,250);
      ras.line_to_d(w,h);
      ras.line_to_d(0,h);
      agg::render_scanlines_aa(ras, sl, rbase, sa, sg);

      exitMenu.transform(shape_mtx);
      exitApp.transform(shape_mtx);
      newGame.transform(shape_mtx);
      p1_ctr.transform(shape_mtx);
      p2_ctr.transform(shape_mtx);
      agg::render_ctrl(ras, sl, rbase, exitMenu);
      agg::render_ctrl(ras, sl, rbase, exitApp);
      agg::render_ctrl(ras, sl, rbase, newGame);
      agg::render_ctrl(ras, sl, rbase, p1_ctr);
      agg::render_ctrl(ras, sl, rbase, p2_ctr);
      agg::render_ctrl(ras, sl, rbase, sound);
      agg::render_ctrl(ras, sl, rbase, music);

      // print scores
      char string[200];
      sprintf(string, "Scores:\n\n\tHuman: %d\t Computer: %d", app.hPoints, app.cPoints);
      app.draw_text(40, 300, 15*scale, string);
   }
   virtual void on_ctrl_change()
   {
      if (exitMenu.status())
      {
         exitMenu.status(false);
         app.changeView("game");
      }
      if (newGame.status())
      {
         newGame.status(false);
         app.changeView("game");
         Pentago_ai new_pentago;
         app.pentago = new_pentago;
      }
      if (exitApp.status())
      {
         throw 0;
      }
      p1_ctr.label(!p1_ctr.value()? "P1: H": "P1: C(%1.0f)");
      p2_ctr.label(!p2_ctr.value()? "P2: H": "P2: C(%1.0f)");
      app.p1level = p1_ctr.value();
      app.p2level = p2_ctr.value();
      app.sound_on(sound.status());
      app.music_on(music.status());
   }
   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

private:
    App& app;
    agg::button_ctrl<agg::rgba8> exitApp;
    agg::button_ctrl<agg::rgba8> exitMenu;
    agg::button_ctrl<agg::rgba8> newGame;
    agg::cbox_ctrl<agg::rgba8>   music;
    agg::cbox_ctrl<agg::rgba8>   sound;
    agg::slider_ctrl<agg::rgba8> p1_ctr;
    agg::slider_ctrl<agg::rgba8> p2_ctr;
};
