#ifndef PENTAGO_H
#define PENTAGO_H

#include <stdio.h>
#include <stack>
#include <string>
#include <sstream>

#define SIZE 6
#define SUB_SQUARES_NR 4
#define SUB_SQUARES_SZ 3

typedef char Point;
#define EMPTY 0
#define BLACK 1
#define WHITE 2

#define ROTATE_LEFT 0
#define ROTATE_RIGH 1

struct Move {
   int first;
   int second;
   int square;
   int rotate; 
   int it;
   Move(int x, int y, int s, int r): first(x), second(y), square(s), rotate(r) {}
   Move() {}
   int x() {return first;}
   int y() {return second;}
   std::string to_str() { std::stringstream ss; ss << "Move:" << first << "," << second << " sq:" << square << (rotate == ROTATE_LEFT? " L": " R"); return ss.str();}
};

class MoveNotValid {};

class Pentago
{
protected:
   int scoreP0, scoreP1;

public:
   int moveNr; 
   Point points[SIZE][SIZE];
   bool shouldMove;
   Pentago():
      scoreP0(0), scoreP1(0), moveNr(0)
   {
      shouldMove = true;
      for (int i = 0; i < SIZE; i++)
         for (int j = 0; j < SIZE; j++)
            points[i][j] = EMPTY;
      winnigRow[0] = Move(0, 0, 0, 0);
      winnigRow[1] = Move(0, 0, 0, 0);
      winnigRow[2] = Move(0, 0, 0, 0);
      winnigRow[3] = Move(0, 0, 0, 0);
      winnigRow[4] = Move(0, 0, 0, 0);
   }
   Pentago(const Pentago& other)
   {
      *this = other;
   }

   int getPoint(int x, int y) {return points[x][y];}
   int getScoreP1() { return scoreP0; }
   int getScoreP2() { return scoreP1; }
   bool endOfTheGame() {return haveFive(BLACK) || haveFive(WHITE) || noMoreEmpty(); }

   inline bool inRange(int x, int y)
   {
      return (x >= 0 && y >= 0 && x < SIZE && y < SIZE);
   }

   bool noMoreEmpty()
   {
      for (int i = 0; i < SIZE; i++)
         for (int j = 0; j < SIZE; j++)
            if (points[i][j] == EMPTY) return false;
      return true;
   }

   inline bool haveFive(int x, int y, Point p)
   {
      if (points[x][y] != p)
         return false;

      if (inRange(x, y+4)
             && points[x][y+1] == p
             && points[x][y+2] == p
             && points[x][y+3] == p
             && points[x][y+4] == p)
      {
         winnigRow[0] = Move(x, y+0, 0, 0);
         winnigRow[1] = Move(x, y+1, 0, 0);
         winnigRow[2] = Move(x, y+2, 0, 0);
         winnigRow[3] = Move(x, y+3, 0, 0);
         winnigRow[4] = Move(x, y+4, 0, 0);
         return true;
      }
      if (inRange(x+4, y+4)
             && points[x+1][y+1] == p
             && points[x+2][y+2] == p
             && points[x+3][y+3] == p
             && points[x+4][y+4] == p)
      {
         winnigRow[0] = Move(x+0, y+0, 0, 0);
         winnigRow[1] = Move(x+1, y+1, 0, 0);
         winnigRow[2] = Move(x+2, y+2, 0, 0);
         winnigRow[3] = Move(x+3, y+3, 0, 0);
         winnigRow[4] = Move(x+4, y+4, 0, 0);
         return true;
      }
      if (inRange(x+4, y)
            && points[x+1][y] == p
            && points[x+2][y] == p
            && points[x+3][y] == p
            && points[x+4][y] == p)
      {
         winnigRow[0] = Move(x+0, y, 0, 0);
         winnigRow[1] = Move(x+1, y, 0, 0);
         winnigRow[2] = Move(x+2, y, 0, 0);
         winnigRow[3] = Move(x+3, y, 0, 0);
         winnigRow[4] = Move(x+4, y, 0, 0);
         return true;
      }
      if (inRange(x-4, y+4)
            && points[x-1][y+1] == p
            && points[x-2][y+2] == p
            && points[x-3][y+3] == p
            && points[x-4][y+4] == p)
      {
         winnigRow[0] = Move(x-0, y+0, 0, 0);
         winnigRow[1] = Move(x-1, y+1, 0, 0);
         winnigRow[2] = Move(x-2, y+2, 0, 0);
         winnigRow[3] = Move(x-3, y+3, 0, 0);
         winnigRow[4] = Move(x-4, y+4, 0, 0);
         return true;
      }
      return false;
   }
   bool haveFive(Point p)
   {
      for (int i = 0; i <= SIZE-5; i++)
         for (int j = 0; j < SIZE; j++)
         {
            if (haveFive(i, j, p))
               return true;
         }
      for (int i = SIZE-5+1; i < SIZE; i++)
         for (int j = 0; j <= SIZE-5; j++)
         {
            if (haveFive(i, j, p))
               return true;
         }
      return false;
   }

   void move_valid(int x, int y)
   {
      if (not shouldMove or not inRange(x, y) or points[x][y] != EMPTY)
         throw MoveNotValid();
   }

   Point currentPlayer()
   {
      return (moveNr%2)? BLACK: WHITE;
   }

   int* currentPlayerScore()
   {
      return (moveNr%2 == 0)? &scoreP0 : &scoreP1;
   }

   void move(int x, int y)
   {
      move_valid(x, y);
      points[x][y] = currentPlayer();
      shouldMove = false;
   }

   inline int topYSquare(int sq)
   {
      switch (sq)
      {
         case 0:
            return 0;
         case 1:
            return 3;
         case 2:
            return 0;
         case 3:
         default:
            return 3;
      }
   }
   inline int topXSquare(int sq)
   {
      switch (sq)
      {
         case 0:
            return 0;
         case 1:
            return 0;
         case 2:
            return 3;
         case 3:
         default:
            return 3;
      }
   }

   void rotateRight(int squareId)
   {
      if (shouldMove or squareId < 0 or squareId >= SUB_SQUARES_NR)
         throw MoveNotValid();
      shouldMove = true;
      moveNr++;
      Point rotated[SUB_SQUARES_SZ][SUB_SQUARES_SZ];
      for (int i = 0; i < SUB_SQUARES_SZ; i++)
         for (int j = 0; j < SUB_SQUARES_SZ; j++)
         {
            rotated[i][j] = points[topXSquare(squareId)+j][2-i+topYSquare(squareId)];
         }
      for (int i = 0; i < SUB_SQUARES_SZ; i++)
         for (int j = 0; j < SUB_SQUARES_SZ; j++)
         {
            int si = topXSquare(squareId)+i; 
            int sj = topYSquare(squareId)+j; 
            points[si][sj] = rotated[i][j];
         }
   }
   void rotateLeft(int squareId)
   {
      if (shouldMove or squareId < 0 or squareId >= SUB_SQUARES_NR)
         throw MoveNotValid();
      shouldMove = true;
      moveNr++;
      
      Point rotated[SUB_SQUARES_SZ][SUB_SQUARES_SZ];
      for (int i = 0; i < SUB_SQUARES_SZ; i++)
         for (int j = 0; j < SUB_SQUARES_SZ; j++)
         {
            rotated[i][j] = points[2-j+topXSquare(squareId)][i+topYSquare(squareId)];
         }
      for (int i = 0; i < SUB_SQUARES_SZ; i++)
         for (int j = 0; j < SUB_SQUARES_SZ; j++)
         {
            int si = topXSquare(squareId)+i; 
            int sj = topYSquare(squareId)+j; 
            points[si][sj] = rotated[i][j];
         }
   }

   void move(Move& m)
   {
      move(m.first, m.second);
      if (m.rotate == ROTATE_LEFT)
         rotateLeft(m.square);
      else
         rotateRight(m.square);
   }

   std::string to_str(int l = 0)
   {
      std::stringstream ss;
      for (int i = 0; i < 6; i++)
      {
         ss << std::string(2*l, ' ');
         for (int j = 0; j < 6; j++)
         {
            ss <<
               (points[i][j]==EMPTY? " ":
               points[i][j]==BLACK? "X":
               "O");
         }
         ss << "\n";
      }
      return ss.str();
   }

   void undo(Move m)
   {
      if (m.square != -1)
      {
         shouldMove = false;
         moveNr--;
         if (m.rotate == ROTATE_LEFT)
            rotateRight(m.square);
         if (m.rotate == ROTATE_RIGH)
            rotateLeft(m.square);
         if (moveNr)
            moveNr--;
      }
      shouldMove = true;
      points[m.first][m.second] = EMPTY;
   }

   Move winnigRow[5];
};

#endif
