#include "agg_arrowhead.h"
#include "agg_conv_stroke.h"
#include "agg_conv_dash.h"
#include "agg_conv_curve.h"
#include "agg_conv_contour.h"
#include "agg_conv_marker.h"
#include "agg_conv_shorten_path.h"
#include "agg_conv_marker_adaptor.h"
#include "agg_conv_concat.h"
#include "agg_arrowhead.h"
#include "agg_vcgen_markers_term.h"

//============================================================================
struct curve
{
    agg::curve3 c;

    curve(double x1, double y1, double x2, double y2, double x3, double y3, double k=0.5)
    {
        c.init(x1, y1, 
               x2, y2,
               x3, y3);
    }

    void rewind(unsigned path_id) { c.rewind(path_id); }
    unsigned vertex(double* x, double* y) { return c.vertex(x, y); }
};


//============================================================================
template<class Source> struct stroke_fine_arrow
{
    typedef agg::conv_stroke<Source, agg::vcgen_markers_term>                   stroke_type;
    typedef agg::conv_marker<typename stroke_type::marker_type, agg::arrowhead> marker_type;
    typedef agg::conv_concat<stroke_type, marker_type>                          concat_type;

    stroke_type    s;
    agg::arrowhead ah;
    marker_type    m;
    concat_type    c;

    stroke_fine_arrow(Source& src, double w) : 
        s(src),
        ah(),
        m(s.markers(), ah),
        c(s, m)
    {
        s.width(w); 
        ah.head(20, 10, 10, 5);
        s.shorten(w * 2.0);
    }

    void rewind(unsigned path_id) { c.rewind(path_id); }
    unsigned vertex(double* x, double* y) { return c.vertex(x, y); }
};


inline void drawArrow(double x1, double y1, double x2, double y2, double x3, double y3, double width, agg::rasterizer_scanline_aa<>& ras)
{
   curve c(x1, y1, x2, y2, x3, y3);
   stroke_fine_arrow<curve> s(c, width);
   ras.add_path(s);
}
