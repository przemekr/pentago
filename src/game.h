#include "arrow_curve.h"
#include "agg_rounded_rect.h"

static int aiProgress;
static int aiExpected;
extern long noEval;
extern int rating;

CbResult callback(void* callBackData)
{
   App* app = (App*)callBackData;
   aiProgress++;
   if (app->elapsed_time() < 100)
   {
      return CONTINUE;
   }
   app->start_timer();
   app->on_draw();
   app->update_window();
   return CONTINUE;
}

inline static void swapWhen(bool cond, double& x1, double& y1, double& x2, double& y2)
{
   if (not cond)
      return;

   double x = x1; x1 = x2; x2 = x;
   double y = y1; y1 = y2; y2 = y;
}

class line
{
public:
   line(double x1, double y1, double x2, double y2, double thickness) :
      m_x1(x1), m_y1(y1), m_x2(x2), m_y2(y2), m_thickness(thickness) { }

   void rewind(unsigned)
   {
      agg::calc_orthogonal(m_thickness*0.5, m_x1, m_y1, m_x2, m_y2, &m_dx, &m_dy);
      m_vertex = 0;
   }

   unsigned vertex(double* x, double* y)
   {
      switch(m_vertex)
      {
         case 0:
            *x = m_x1 - m_dx;
            *y = m_y1 - m_dy;
            m_vertex++;
            return agg::path_cmd_move_to;

         case 1:
            *x = m_x2 - m_dx;
            *y = m_y2 - m_dy;
            m_vertex++;
            return agg::path_cmd_line_to;

         case 2:
            *x = m_x2 + m_dx;
            *y = m_y2 + m_dy;
            m_vertex++;
            return agg::path_cmd_line_to;

         case 3:
            *x = m_x1 + m_dx;
            *y = m_y1 + m_dy;
            m_vertex++;
            return agg::path_cmd_line_to;
      }
      return agg::path_cmd_stop;
   }
private:
   double   m_x1;
   double   m_y1;
   double   m_x2;
   double   m_y2;
   double   m_dx;
   double   m_dy;
   double   m_thickness;
   unsigned m_vertex;
};

class GameView : public View
{
public:
   GameView(App& application): app(application),
      menu(80,  20, 180, 40,   "MENU", !flip_y),
      undo(220, 20, 320, 40,   "UNDO", !flip_y),
      animation(50)
   {
      lx = ly = 0;
      rotatation = false;
      downX = 0;
      downY = 0;
      currX = 0;
      currY = 0;

      undo.background_color(red);
      menu.background_color(red);

      add_ctrl(undo);
      add_ctrl(menu);
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      if (key == 27 || key == 0x4000010e /* Android BACK key*/)
      {
         undo.status(true);
         on_ctrl_change();
      }
   }

   virtual void on_ctrl_change()
   {
      app.wait_mode(true);
      if (app.pentago.endOfTheGame())
      {
         animation = 0;
      }
      if ((app.pentago.currentPlayer() == WHITE && app.p1level)
            || (app.pentago.currentPlayer() == BLACK && app.p2level))
      {
         app.wait_mode(false);
      }

      if (undo.status())
      {
         animation = 20;
         undo.status(false);
         if (undoList.empty())
            return;
         Move m = undoList.top(); undoList.pop();
         app.pentago.undo(m);

         if ((app.pentago.currentPlayer() == WHITE && app.p1level)
               || (app.pentago.currentPlayer() == BLACK  && app.p2level))
         {
            if (undoList.empty())
               return;
            m = undoList.top(); undoList.pop();
            app.pentago.undo(m);
         }
      }

      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }
      app.force_redraw();
   }

   virtual void on_resize(int, int)
   {
      app.force_redraw();
      double w = app.rbuf_window().width();
      double h = app.rbuf_window().height();
      size = int(std::min(w*0.95, h*0.9));
      hshift = h - size;
      hshift -= hshift/4;
      wshift = w - size;
      wshift /= 2;
   }

   void enter()
   {
      app.play_music(rand()%4, 40);
      app.wait_mode(false);
   }

   virtual void on_idle()
   {
#define MAX_FPS 25
      if (animation-- > 0)
      {
         int loop_time = app.elapsed_time();
         if (loop_time < 1000/MAX_FPS)
         {
            usleep(1000*(1000/MAX_FPS-loop_time));
         }
         app.start_timer();
         app.force_redraw();
         app.wait_mode(false);
         return;
      }

      app.wait_mode(true);
      if (app.pentago.endOfTheGame())
      {
         return;
      }

      if ((app.pentago.currentPlayer() == WHITE && !app.p1level)
            || (app.pentago.currentPlayer() == BLACK && !app.p2level))
      {
         return;
      }

      app.start_timer();
      aiExpected = app.pentago.noOfPossibleMoves();
      Move m = ai_move(app.pentago, app.pentago.currentPlayer() == WHITE?
            app.p1level : app.p2level, callback, (void*)&app);
      app.pentago.move(m);
      undoList.push(m);
      aiProgress = 0;
      animation = app.pentago.endOfTheGame()? 50: 20;
      app.cPoints += app.pentago.endOfTheGame()? 1: 0;
      app.wait_mode(false);
      app.force_redraw();
      app.play_sound(
            app.pentago.endOfTheGame()? 2:
            app.pentago.currentPlayer() == WHITE? 0:1,
            500);

   }

   int pixToX(int x)
   {
      x -= wshift;
      if (x < 0) return -1;

      double wsize = size/(SIZE);
      return x/wsize;
   }
   int pixToY(int y)
   {
      y -= hshift;
      if (y < 0) return -1;

      double hsize = size/(SIZE);
      return y/hsize;
   }

   int getClosestSquer(int x, int y)
   {
      x -= wshift;
      y -= hshift;
      double wsize = size/(sqrt((double)SUB_SQUARES_NR));
      int indx = (int)(x/wsize);
      int indy = (int)(y/wsize);
      return indy + indx*sqrt((double)SUB_SQUARES_NR);
   }

   double distance(int x1, int y1, int x2, int y2)
   {
      return sqrt((double)(x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
   }

   int angle(int x1, int y1, int cx, int cy, int x2, int y2)
   {
      x1 = x1- cx;
      y1 = y1- cy;
      x2 = x2- cx;
      y2 = y2- cy;
      double a = 180.0/agg::pi*(atan2((double)x1, y1) - atan2((double)x2, y2));
      return a < 0? a + 360: a;
   }

   void squeraCenter(int sq, int& x, int& y)
   {
      double wsize = size/(sqrt((double)SUB_SQUARES_NR));
      x = wshift+ wsize * (0.5+ sq /(int)(sqrt((double)SUB_SQUARES_NR)));
      y = hshift+ wsize * (0.5+ sq %(int)(sqrt((double)SUB_SQUARES_NR)));
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
      rotatation = false;

      /* check if rotate */
      int sx, sy;
      int square = getClosestSquer(x, y);
      squeraCenter(square, sx, sy);
      int ang = angle(downX,downY, sx,sy, x,y);
      try {
         if (getClosestSquer(downX, downY) == square
               and distance(downX,downY, x,y) >= 20
               and (30 <= ang and ang <= 150))
         {
            app.pentago.rotateRight(square);
            undoList.top().rotate = ROTATE_RIGH;
            undoList.top().square = square;
         }
         else if (getClosestSquer(downX, downY) == square
               and distance(downX,downY, x,y) >= 20
               and 230 <= ang and ang <= 330)
         {
            app.pentago.rotateLeft(square);
            undoList.top().rotate = ROTATE_LEFT;
            undoList.top().square = square;
         }
         else
         {
            app.pentago.move(pixToX(x), pixToY(y));
            undoList.push(Move(pixToX(x),pixToY(y), -1,-1));
         }

         animation     = app.pentago.endOfTheGame()? 50: 20;
         app.hPoints +=  app.pentago.endOfTheGame()? 1: 0;
         app.play_sound(
               app.pentago.endOfTheGame()? 2:
               app.pentago.currentPlayer() == WHITE? 0:1,
               500);
      } catch (MoveNotValid& m) {}
      app.force_redraw();
      app.wait_mode(false);
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
      downX = x;
      downY = y;
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }

      if (!flags & agg::mouse_left)
         return;

      rotatation = true;
      currX = x;
      currY = y;
      app.force_redraw();
   }
   virtual void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
   }

   bool isInSubSquare(int sqId, int i, int j)
   {
      return i >= app.pentago.topXSquare(sqId) and i < app.pentago.topXSquare(sqId)+3
         and j >= app.pentago.topYSquare(sqId) and j < app.pentago.topYSquare(sqId)+3;
   }

   void draw_subsq(renderer_base_type& rbase,
         double x1, double y1,
         double x2, double y2,
         agg::trans_affine& m)
   {
      agg::rounded_rect s1(x1,y1,x2,y2, 8.0);
      agg::rounded_rect s2(x1+5,y1+5,x2-5,y2-5, 8.0);
      agg::rounded_rect s3(x1+5,y1+5,x2-5,y2-5, 8.0);
      agg::conv_transform<agg::rounded_rect> sq1(s1, m);
      agg::conv_transform<agg::rounded_rect> sq2(s2, m);
      agg::conv_transform<agg::rounded_rect> sq3(s2, m);

      double s = 1.0*size/(SIZE);

      line lh1(x1+5, y1+s,   x2-5, y1+s,     0.5);
      line lh2(x1+5, y1+2*s, x2-5, y1+2*s,   0.5);
      line lv1(x1+1*s, y1+5, x1+1*s, y2-5,   0.5);
      line lv2(x1+2*s, y1+5, x1+2*s, y2-5,   0.5);
      agg::conv_transform<line> l1(lh1, m);
      agg::conv_transform<line> l2(lh2, m);
      agg::conv_transform<line> l3(lv1, m);
      agg::conv_transform<line> l4(lv2, m);

      agg::rasterizer_scanline_aa<> ras1;
      agg::rasterizer_scanline_aa<> ras2;
      ras1.filling_rule(agg::fill_even_odd);

      agg::scanline_u8 sl;
      renderer_scanline_type ren1(rbase);
      renderer_scanline_type ren2(rbase);
      renderer_scanline_type ren3(rbase);

      const agg::rgba dgreen(0.6, 0.6, 0,  0.8);
      const agg::rgba lgreen(0.6, 0.3, 0,  0.5);
      ren1.color(lgreen);
      ren2.color(dgreen);
      ren3.color(lblue);

      ras1.add_path(sq1);
      ras1.add_path(sq2);
      ras2.add_path(sq3);
      agg::render_scanlines(ras1, sl, ren1);
      agg::render_scanlines(ras2, sl, ren2);

      ras2.reset();
      ras2.add_path(l1);
      ras2.add_path(l2);
      ras2.add_path(l3);
      ras2.add_path(l4);
      agg::render_scanlines(ras2, sl, ren3);
   }

   virtual void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      ras.filling_rule(agg::fill_even_odd);
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(lgray);
      renderer_scanline_type ren_sl(rbase);

      int sx, sy;
      int square = getClosestSquer(downX, downY);
      squeraCenter(square, sx, sy);
      int ang = angle(downX,downY, sx,sy, currX,currY);
      agg::trans_affine I; I.reset();
      static agg::trans_affine m; m.reset();
      m *= agg::trans_affine_translation(-sx, -sy);
      m *= agg::trans_affine_rotation(rotatation? agg::pi*ang/180: 0);
      m *= agg::trans_affine_translation(sx, sy);

      const agg::rgba lgreen(0,1,0,0.6);
      ren_sl.color(lgreen);

      for (int i = 0; i < 4; i++)
      {
         int x = i /2;
         int y = i %2;
         double x1 = wshift+2 + x*(size/2.0-2);
         double y1 = hshift+2 + y*(size/2.0-2);
         double x2 = x1+(size/2)-4;
         double y2 = y1+(size/2)-4;
         draw_subsq(rbase, x1, y1, x2, y2, i == square? m: I);
      }

      double s = 1.0*size/(SIZE);

      for (int i = 0; i < SIZE; i++)
         for (int j = 0; j < SIZE; j++)
         {
            if (app.pentago.getPoint(i, j) == EMPTY)
               continue;

            ren_sl.color(app.pentago.getPoint(i, j) == WHITE?
                  agg::rgba(1, 1, 1, 0.9):
                  agg::rgba(0.1, 0.1, 0.1, 0.9));
            agg::ellipse e;
            ras.reset();
            double ex = 0.5*s+wshift+s*i;
            double ey = 0.5*s+hshift+s*j;
            if (isInSubSquare(square, i, j))
            {
               m.transform(&ex, &ey);
            }
            e.init(ex, ey, s*0.4, s*0.4, 128);
            ras.add_path(e);
            agg::render_scanlines(ras, sl, ren_sl);
         }

      if (animation && app.pentago.endOfTheGame())
      {
         ren_sl.color(agg::rgba(1, 0, 0, 0.8/20*animation));
         ras.reset();
         for (int i = 0; i < 5; i++)
         {
            agg::ellipse e;
            e.init(wshift+s*0.5+s*app.pentago.winnigRow[i].first,
                   hshift+s*0.5+s*app.pentago.winnigRow[i].second,
                   s*0.4, s*0.4, 128);
            ras.add_path(e);
         }
         agg::render_scanlines(ras, sl, ren_sl);
      } else if (animation && not undoList.empty())
      {
         ren_sl.color(agg::rgba(1, 0, 0, 0.8/20*animation));
         ras.reset();
         agg::ellipse e;
         e.init(
               0.5*s+wshift+s*undoList.top().first,
               0.5*s+hshift+s*undoList.top().second,
               s*0.4, s*0.4, 128);
         ras.add_path(e);

         if (undoList.top().square != -1)
         {
            double mx,my,sx,sy,ex,ey;
            int r = undoList.top().rotate;
            switch (undoList.top().square)
            {
               case 0:
                  mx = wshift;
                  my = hshift;
                  sx = mx;
                  sy = my + 50;
                  ex = mx + 50;
                  ey = my;
                  swapWhen(r == ROTATE_LEFT, sx,sy, ex,ey);
                  break;
               case 1:
                  mx = wshift;
                  my = hshift+size;
                  sx = mx + 50;
                  sy = my;
                  ex = mx;
                  ey = my - 50;
                  swapWhen(r == ROTATE_LEFT, sx,sy, ex,ey);
                  break;
               case 2:
                  mx = wshift+size;
                  my = hshift;
                  sx = mx - 50;
                  sy = my;
                  ex = mx;
                  ey = my + 50;
                  swapWhen(r == ROTATE_LEFT, sx,sy, ex,ey);
                  break;
               default:
               case 3:
                  mx = wshift+size;
                  my = hshift+size;
                  sx = mx;
                  sy = my-50;
                  ex = mx-50;
                  ey = my;
                  swapWhen(r == ROTATE_LEFT, sx,sy, ex,ey);
                  break;
            }
            drawArrow(sx,sy, mx,my, ex,ey, 8.0, ras);
            agg::render_scanlines(ras, sl, ren_sl);
         }
      }

      if (!app.pentago.endOfTheGame())
         app.draw_text(20.0, 100.0, 10.0,
               black, 1.0,
               "%s should: %s",
               app.pentago.currentPlayer() == WHITE? "P1 (WHITE)": "P2 (BLACK)",
               app.pentago.shouldMove? "select": "rotate");
      else if (app.pentago.haveFive(WHITE))
         app.draw_text(20.0, 100.0, 10.0,
               black, 1.0,
               "P1 (WHITE) wins!");
      else if (app.pentago.haveFive(BLACK))
         app.draw_text(20.0, 100.0, 10.0,
               black, 1.0,
               "P2 (BLACK) wins!");
      else
         app.draw_text(20.0, 100.0, 10.0,
               black, 1.0,
               "The game is a draw...");

      if (aiProgress)
      {
         app.draw_text(20.0, 50.0, 10.0,
               red, 1.0, "AI: %d%% (%d, %ldK)", aiProgress*100/aiExpected, rating, noEval/1000);
      }

      double scale = app.rbuf_window().width()/400.0;
      static agg::trans_affine shape_mtx; shape_mtx.reset();
      shape_mtx *= agg::trans_affine_scaling(scale);
      shape_mtx *= agg::trans_affine_translation(0, 0);
      undo.transform(shape_mtx);
      menu.transform(shape_mtx);

      agg::render_ctrl(ras, sl, rbase, undo);
      agg::render_ctrl(ras, sl, rbase, menu);
   }
private:
    App& app;
    std::stack<Move> undoList;
    int size, wshift, hshift;
    int lx, ly;
    agg::button_ctrl<agg::rgba8> menu;
    agg::button_ctrl<agg::rgba8> undo;
    int animation;
    bool rotatation;
    int downX, downY;
    int currX, currY;
};
