#ifndef AI_MINMAX_SEARCH_H
#define AI_MINMAX_SEARCH_H

#include <stdlib.h>
#include <time.h>
#include <new>
#include <stdio.h>
#include <iostream>
#include <time.h>

#define NO_MOVE_LIST (0xFFFF)
#define NO_GRAD      (0xFFFF)
#define MIN_GRAD     (-1024)
#define MAX_GRAD     ( 1024)

enum CbResult {
   CONTINUE,
   HURRY_UP,
   STOP_NOW
};
typedef CbResult (*CbFun)(void*);

extern long noEval;
extern int rating;
static int limit;
static int reduced;

template <typename B> 
static int max(B board, int level, int alpha, int beta);

template <typename B> 
static int min(B board, int level, int alpha, int beta);

template <typename B, typename M> 
static int max(B board, M& m, CbFun cbFun, void* cbData)
{
   int value = -128;
   int alpha = -128;
   int beta  = 128;
   int level = 0;

   for (typename B::iterator i = board.begin(); i != board.end(); i++)
   {
      B newb(board);
      newb.move(*i);
      int moveVal = min(newb, level+1, alpha, beta);
      if (moveVal > value)
      {
         value = moveVal;
         rating = value;
         m = *i;
      }
      alpha = std::max(alpha, value);
      cbFun(cbData);
      //std::cout << (*i).to_str() << " value " << moveVal << "\n";
   }
   return value;
}

template <typename B> 
static int min(B board, int level, int alpha, int beta)
{
   int value = 128;
   if (level >= limit)
   {
      noEval++;
      int v = board.evaluate(level);
#ifdef DEBUG_AI
      std::cout << std::string(2*level, ' ') << "eval:" << v << "\n";
      std::cout << board.to_str(level) << "\n";
#endif
      return v;
   }

   bool possibleMoves = false;
   for (typename B::iterator i = board.begin(level>=reduced); i != board.end(); i++)
   {
      possibleMoves = true;
      B newb(board);
      newb.move(*i);
      value = std::min(max(newb, level+1, alpha, beta), value);
      beta  = std::min(beta, value);
#ifdef DEBUG_AI
      std::cout << std::string(level*2, ' ') << (*i).to_str() << " v:" << value << std::endl;
#endif
      if (alpha >= beta)
         break;
   }
   if (not possibleMoves)
   {
      noEval++;
      int v = board.evaluate(level);
#ifdef DEBUG_AI
      std::cout << std::string(2*level, ' ') << "eval:" << v << "\n";
      std::cout << board.to_str(level) << "\n";
#endif
      return v;
   }
   return value;
}

template <typename B> 
static int max(B board, int level, int alpha, int beta)
{
   int value = -128;
   if (level >= limit)
   {
      noEval++;
      int v = board.evaluate(level);
#ifdef DEBUG_AI
      std::cout << std::string(2*level, ' ') << "eval:" << v << "\n";
      std::cout << board.to_str(level) << "\n";
#endif
      return v;
   }

   bool possibleMoves = false;
   for (typename B::iterator i = board.begin(level>=reduced); i != board.end(); i++)
   {
      possibleMoves = true;
      B newb(board);
      newb.move(*i);
      value = std::max(min(newb, level+1, alpha, beta), value);
      alpha = std::max(alpha, value);
#ifdef DEBUG_AI
      std::cout << std::string(level*2, ' ') << (*i).to_str() << " v:" << value << std::endl;
#endif
      if (alpha >= beta)
         break;
   }
   if (not possibleMoves)
   {
      noEval++;
      int v = board.evaluate(level);
#ifdef DEBUG_AI
      std::cout << std::string(2*level, ' ') << "eval:" << v << "\n";
      //std::cout << board.to_str(level) << "\n";
#endif
      return v;
   }
   return value;
}

template <typename B, typename M> 
void ai_min_max(B board, M& m, int l, int r, CbFun cbfun, void* cbData)
{
   srand(time(NULL));
   limit = l;
   reduced = r;
   int value = max(board, m, cbfun, cbData);
   printf("%s  noEval:%ld, rating %d\n", m.to_str().c_str(), noEval, value);
}
#endif
