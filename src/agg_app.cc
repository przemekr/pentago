/*
 * Pentago game.
 * Copyright 2013 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 *
 * This file is part of Pentago.
 *
 * Pentago is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pentago is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Pentago.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pentago.h"
#include "pentago_ai.h"
#include "app_support.h"
#include "game.h"
#include "menu.h"


class the_application: public App
{
public:
   the_application(agg::pix_format_e format, bool flip_y) :
      App(format, flip_y)
   {
      game = new GameView(*this);
      menu = new MenuView(*this);
      view = game;
   }
   virtual void changeView(const char* name) 
   {
      if (strcmp(name, "menu") == 0)
         view = menu;
      if (strcmp(name, "game") == 0)
         view = game;
      view->enter();
   };
private:
   GameView* game;
   MenuView* menu;
};


int agg_main(int argc, char* argv[])
{
    the_application app(agg::pix_format_bgra32, flip_y);
    app.caption("PentagoAGG");

    if (false
          || !app.load_img(0,   "pentago.png")
          || !app.load_music(0, "music_track_1.ogg")
          || !app.load_music(1, "music_track_2.ogg")
          || !app.load_music(2, "music_track_3.ogg")
          || !app.load_music(3, "music_track_4.ogg")
          || !app.load_sound(0, "beep1.ogg")
          || !app.load_sound(1, "beep2.ogg")
          || !app.load_sound(2, "applouse.ogg")
       )
    {
        char buf[256];
        sprintf(buf, "There must be files 1%s...9%s\n",
                     app.img_ext(), app.img_ext());
        app.message(buf);
        return 1;
    }

    if (app.init(START_W, START_H, WINDOW_FLAGS))
    {
       try {
          return app.run();
       } catch (...) {
          return 0;
       }
    }
    return 1;
}
